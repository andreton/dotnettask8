﻿using System;
using System.Collections.Generic;

namespace DotNetTask8
{
    class Program
    {
        static void Main(string[] args)
        {
  
            Animal bird1 = new Bird("White-mountain-Owl", "North-America", "white"); //  Making some animal object
            Animal bird2 = new Bird("Crow", "Nepal", "Black");
            Animal fish1 = new Fish("Tuna", "South Korea", "grey", "large"); // Making an owl with overridden constructor
            Animal fish2 = new Fish("Blue whale", "India", "Blue", "huge");
            Animal fish3 = new Fish("Sardina", "10", "12");
           

            Stack<Animal> animalStack = new Stack<Animal>(); //Putting the animals in to a stack
            animalStack.Push(bird1);
            animalStack.Push(bird2);
            animalStack.Push(fish1);
            animalStack.Push(fish2);
            animalStack.Push(fish3);

            foreach (var stackItem in animalStack) //iterate through the items in the stack 
            {
                Console.WriteLine(stackItem.ToString());
            }
            fish1.isFishNemo(); //Calling method from fish class
        }
    }
}
