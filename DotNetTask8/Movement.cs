﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask8
{
    public interface Movement // Interface that has a method that is needed to be implemented
    {
        public void IMovement();
      
    }
}
